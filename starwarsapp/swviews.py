import json
import requests

from flask import (
    Blueprint, current_app, Response, abort, request, url_for
)

from bson import json_util
from bson.objectid import ObjectId
import bson

from werkzeug.exceptions import BadRequest

from . import cache

blueprint = Blueprint('starwars', __name__, url_prefix='/')


def get_planets_collection():
    """Retrieve a cursor for planets collection from database."""
    return current_app.mongo.db["Planets"]

@cache.cache.cached(timeout=86400)
def get_swapi_planets_json():
    """Retrieve planets json from swapi."""
    return json.loads(requests.get('https://swapi.co/api/planets').text)

@blueprint.route('/planets', methods=['GET'])
def get_all_planets():
    """Retrieves all planets from database."""
    planets_collection = get_planets_collection()
    planets_documents = planets_collection.find()

    json_response = []
    for document in planets_documents:
        json_response.append(document)

    return Response(
        json_util.dumps(json_response),
        mimetype='application/json'
    ), 200


@blueprint.route('/planets/name/<planet_name>', methods=['GET'])
def get_planet_by_name(planet_name):
    """Retrieves a planet from database based on given planet_name."""
    planets_collection = get_planets_collection()
    planet_document = planets_collection.find({"name": planet_name})

    planet_document_count = planets_collection.count_documents({"name": planet_name})

    if planet_document_count == 0:
        return Response(
            json_util.dumps(["Planet with given name not found"]),
            mimetype='application/json'
        ), 404

    return Response(
        json_util.dumps(planet_document),
        mimetype='application/json'
    ), 200


@blueprint.route('/planets/id/<planet_id>', methods=['GET'])
def get_planet_by_id(planet_id):
    """Retrieves a planet from database based on given planet_id."""
    object_id = ""
    try:
        object_id = ObjectId(planet_id)
    except bson.errors.InvalidId as error:
        return Response(
            json_util.dumps(["Planet with given id not found"]),
            mimetype='application/json'
        ), 404

    planets_collection = get_planets_collection()
    planet_document = planets_collection.find({'_id': object_id})

    planet_document_count = planets_collection.count_documents({'_id': object_id})

    if planet_document_count == 0:
        return Response(
            json_util.dumps(["Planet with given id not found"]),
            mimetype='application/json'
        ), 404

    return Response(
        json_util.dumps(planet_document),
        mimetype='application/json'
    ), 200


@blueprint.route('/planets', methods=['POST'])
def insert_planet(planet_name="", climate="", terrain=""):
    """Inserts a planet into database with a planet_name, climate and terrain."""
    planet_name=""
    climate=""
    terrain=""
    film_count = -1

    swapi_json = get_swapi_planets_json()

    if request.method == 'POST':
        request_json = ""

        try:
            request_json = request.get_json()
        except Exception as error:
            return Response(
                json_util.dumps(["Invalid json format"]),
                mimetype='application/json'
            ), 400

        request_json = request.get_json()

        if "name" in request_json:
            planet_name = request_json["name"]
        else:
            return Response(
                json_util.dumps(["Please insert a name for this planet"]),
                mimetype='application/json'
            ), 400

        if "climate" in request_json:
            climate = request_json["climate"]
        else:
            return Response(
                json_util.dumps(["Please insert a climate for this planet"]),
                mimetype='application/json'
            ), 400

        if "terrain" in request_json:
            terrain = request_json["terrain"]
        else:
            return Response(
                json_util.dumps(["Please insert a terrain for this planet"]),
                mimetype='application/json'
            ), 400

    # Check if "planet_name" name exists in swapi planets database
    for planet in swapi_json["results"]:
        if planet_name == planet["name"]:
            film_count = len(planet["films"])

    if film_count < 0 or not planet_name:
        return Response(
            json_util.dumps(["Planet with given name not found on swapi database"]),
            mimetype='application/json'
        ), 404

    # Check if "planet_name" name already exists in our MongoDB
    planets_collection = get_planets_collection()
    existing_document_count = planets_collection.count_documents({"name": planet_name})

    if existing_document_count > 0:
        return Response(
            json_util.dumps(["Planet with given name already exists"]),
            mimetype='application/json'
        ), 409

    # Insert a new document on MongoDB with our parameters values
    planet_dict = {"name": planet_name,
                   "climate": climate,
                   "terrain": terrain,
                   "filmCount": film_count}
    planets_collection.insert_one(planet_dict)

    return Response(
        json_util.dumps(planet_dict),
        mimetype='application/json'
    ), 200


@blueprint.route('/planets/name/<planet_name>', methods=['DELETE'])
def delete_planet_by_name(planet_name):
    """Deletes a planet from database based on given planet_name."""
    planets_collection = get_planets_collection()
    delete_result = planets_collection.delete_one({"name": planet_name})

    if delete_result.deleted_count == 0:
        return Response(
            json_util.dumps(["Planet with given name not found"]),
            mimetype='application/json'
        ), 404

    return Response(
        json_util.dumps(["Planet deleted"]),
        mimetype='application/json'
    ), 200


@blueprint.route('/planets/id/<planet_id>', methods=['DELETE'])
def delete_planet_by_id(planet_id):
    """Deletes a planet from database based on given planet_id."""
    object_id = ""

    # Check if planet_id is a 12-byte input or a 24-character hex string
    try:
        object_id = ObjectId(planet_id)
    except bson.errors.InvalidId as error:
        return Response(
            json_util.dumps(["Planet with given id not found"]),
            mimetype='application/json'
        ), 404

    planets_collection = get_planets_collection()
    delete_result = planets_collection.delete_one({'_id': object_id})

    if delete_result.deleted_count == 0:
        return Response(
            json_util.dumps(["Planet with given id not found"]),
            mimetype='application/json'
        ), 404

    return Response(
        json_util.dumps(["Planet deleted"]),
        mimetype='application/json'
    ), 200
