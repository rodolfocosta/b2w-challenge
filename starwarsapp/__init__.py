import os

from flask import Flask
from flask_pymongo import PyMongo

from . import swviews
from . import errorhandler
from . import cache

def create_app(test_config=None):
    # create and configure the app
    app = Flask(__name__, instance_relative_config=True)

    if test_config is None:
        # load the instance config, if it exists, when not testing
        app.config.from_pyfile('config.py', silent=True)
    else:
        # load the test config if passed in
        app.config.from_mapping(test_config)

    # ensure the instance folder exists
    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    # configuring MongoDB
    app.config["MONGO_URI"] = "mongodb://localhost:27017/starwarsappDB"
    app.mongo = PyMongo(app)

    # configuring Cache
    cache.cache.init_app(app)

    # all star wars queries (api + database)
    app.register_blueprint(swviews.blueprint)

    # common errors handling
    app.register_error_handler(403, errorhandler.forbidden_access)
    app.register_error_handler(404, errorhandler.page_not_found)
    app.register_error_handler(410, errorhandler.content_gone)
    app.register_error_handler(500, errorhandler.internal_server_error)

    return app
