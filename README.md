# A Star Wars Challenge - B2W 

A REST Star Wars Planets API based on [Stars Wars public API](https://swapi.co/api/) for a B2W Star Wars game.

This API stores Star Wars planets informations such as name, climate, terrain and films count on a MongoDB database.

## Tools

This API was made using the following tools and technologies:

 * [Python 3.7.0](https://www.python.org/)
 * [PyMongo 3.7.1](http://api.mongodb.com/python/current/)
 * [Flask 1.0.2](http://flask.pocoo.org/)
 * [Flask-Caching 1.4.0](https://pythonhosted.org/Flask-Caching/)
 * [MongoDB](http://docs.mongodb.org/manual/installation/)

## API Functionalities

* ### GET /planets
    * **Description:** Retrieves all planets from database.  
    * **Response Code:** 200 (OK).  
    * **Response Data:** Empty JSON (Empty database) or JSON containing planet list (Stored planets).  

* ### GET /planets/name/<planet_name>
    * **Description:** "Retrieves a planet from database based on given *planet_name*.  
    * **Request Parameter:** *planet_name* (String).  
    * **Response Code:** 200 (OK).  
    * **Response Data:** JSON with "Planet not found" message or JSON containing planet informations.  

* ### GET /planets/id/<planet_id>
    * **Description:** Retrieves a planet from database based on given *planet_id*. 
    * **Request Parameter:** *planet_id* (String).  
    * **Response Codes:** 200 (OK), 404 (Not found) - Planet id not found.  
    * **Response Data:** JSON with "Planet not found" message or JSON containing planet informations.  

* ### POST /planets
    * **Description:** Inserts a planet into database with a planet_name, climate and terrain.  
    * **Request body:** JSON containing planet informations (name, climate and terrain).  
Ex: 
    ```
    {
      "name": "Endor",
      "climate": "temperate",
      "terrain": "forests, mountains, lakes"
    }
    ``` 
    * **Response Codes:** 200 (OK) - Successfull insert, 400 (Bad request) - One of the planet informations is missing, 404 (Not found) - Planet name not found in swapi, 409 (Conflict) - Planet name already exists.  
    * **Response Data:** JSON with a message for each error type or JSON containing inserted planet informations.  
Ex: 
    ```
    {
      "name": "Endor",
      "climate": "temperate",
      "terrain": "forests, mountains, lakes",
      "filmCount": 1,
      "_id": {
        "$oid": "5ba2afa9b07fa61a7c6ba2d0"
      }}
    ``` 

* ### DELETE /planets/name/<planet_name>
    * **Description:** Deletes a planet from database based on given *planet_name*.  
    * **Request Parameter:** *planet_name* (String).  
    * **Response Codes:** 200 (OK) - Successfull deletion, 404 (Not found) - Planet name not found.   
    * **Response Data:** JSON with "Planet not found" message or JSON with "Planet deleted" message. 

* ### DELETE /planets/id/<planet_id>
    * **Description:** Deletes a planet from database based on given *planet_id*.  
    * **Request Parameter:** *planet_id* (String).  
    * **Response Codes:** 200 (OK) - Successfull deletion, 404 (Not found) - Planet id not found.   
    * **Response Data:** JSON with "Planet not found" message or JSON with "Planet deleted" message.
 
## Setup and Run (Step-by-Step)

1. Install Python 3.7.0 or latest
2. Install [MongoDB](http://docs.mongodb.org/manual/administration/install-community/) and initialize its service.
3. Open **cmd** (Windows) or **terminal** (Linux and Mac) and go to this project root folder (b2w-challenge)
4. Create a **virtualenv** for this project:
    ```
    python -m venv venv
    ```
5. Enter **virtualenv** by running following command:
    1. **Windows**: 
    ```
    venv\Scripts\activate
    ```
    2. **Linux and Mac**: 
    ```
    source venv/bin/activate
    ```
6. Install required python packages **(under activated virtualenv and on project root folder (b2w-challenge))**  :
    ```
    pip install -r requirements.txt
    ```
7. Run flask application:
    1. **Windows**:     
        1. `set FLASK_APP=starwarsapp`
        2. `set FLASK_ENV=development`
        3. `flask run`
    2. **Linux and Mac**:
        1. `export FLASK_APP=starwarsapp`
        2. `export FLASK_ENV=development`
        3. `flask run`

## Installing and Testing (Step-by-Step)

1. Open **cmd** (Windows) or **terminal** (Linux and Mac) and go to this project root folder (b2w-challenge)
2. Enter **virtualenv** and run:
    ```
    pip install -e .
    ```
3. Go to directory "tests" under this project root folder (b2w-challenge)
4. Run:
    ```
    python test_starwarsapp.py -v
    ```

