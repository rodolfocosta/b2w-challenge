from starwarsapp import create_app
import unittest
import json

from bson import json_util

class StarWarsTester(unittest.TestCase):

    def get_app_client(self):
        return create_app().test_client(self)

    def test_not_found_url_access(self):
        tester = self.get_app_client()
        response = tester.get('/', content_type='application/json')
        self.assertEqual(response.data, b'["Page not found"]')
        self.assertEqual(response.status_code, 404)

    def test_get_all_planets(self):
        tester = self.get_app_client()
        response = tester.get('/planets', content_type='application/json')
        self.assertEqual(response.data, b'[]') # returns True only if database is empty
        self.assertEqual(response.status_code, 200)

    def test_get_planet_by_name(self):
        tester = self.get_app_client()
        response = tester.get('/planets/name/Endor', content_type='application/json')
        self.assertEqual(response.data, b'["Planet with given name not found"]')
        self.assertEqual(response.status_code, 404)

    def test_get_planet_by_id(self):
        tester = self.get_app_client()
        response = tester.get('/planets/id/5ba27436b07fa63958e72f1a', content_type='application/json')
        self.assertEqual(response.data, b'["Planet with given id not found"]')
        self.assertEqual(response.status_code, 404)

    def test_insert_planet(self):
        tester = self.get_app_client()
        response = tester.post('/planets',
                               data=json.dumps(dict(climate="temperate", terrain="forests, mountains, lakes")),
                               content_type='application/json')
        self.assertEqual(response.data, b'["Please insert a name for this planet"]')
        self.assertEqual(response.status_code, 400)

    def test_delete_planet_by_name(self):
        tester = self.get_app_client()
        response = tester.delete('/planets/name/Endor', content_type='application/json')
        self.assertEqual(response.data, b'["Planet with given name not found"]')
        self.assertEqual(response.status_code, 404)

    def test_delete_planet_by_id(self):
        tester = self.get_app_client()
        response = tester.delete('/planets/id/5ba27436b07fa63958e72f1a', content_type='application/json')
        self.assertEqual(response.data, b'["Planet with given id not found"]')
        self.assertEqual(response.status_code, 404)

if __name__ == '__main__':
    unittest.main()
